import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    NgxPermissionsModule,
  ]
})
export class SharedModule { }
