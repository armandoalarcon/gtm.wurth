import { Injectable } from '@angular/core';
import { User } from '../_entities/User';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { Credentials } from '../_entities/Credentials';
import { environment } from 'src/environments/environment';
import { ChangePassword } from '../_entities/ChangePassword';
import { ForgotPassword } from '../_entities/ForgotPassword';
import { ResetPassword } from '../_entities/ResetPassword';
import BaseService from '../base.service';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  private authenticationChanged = new Subject<boolean>();
  private user = new User();
  constructor(private http: HttpClient) {
    super();
  }

  public authenticate(body: Credentials): Observable<any> {
    return this.http.post<any>(environment.API_SECURITY + '/api/account/token', body)
      .pipe(catchError(this.handleError));

  }

  public isAuthenticated(): boolean {
    return (!(sessionStorage.getItem('token') === undefined ||
      sessionStorage.getItem('token') === null ||
      sessionStorage.getItem('token') === 'null' ||
      sessionStorage.getItem('token') === 'undefined' ||
      sessionStorage.getItem('token') === ''));
  }

  public isAuthenticationChanged(): any {
    return this.authenticationChanged.asObservable();
  }

  public getToken(): any {
    if (sessionStorage.getItem('token') === undefined ||
      sessionStorage.getItem('token') === null ||
      sessionStorage.getItem('token') === 'null' ||
      sessionStorage.getItem('token') === 'undefined' ||
      sessionStorage.getItem('token') === '') {
      return '';
    }
    const data = JSON.parse(sessionStorage.getItem('token'));
    return data;

  }

  public setData(data: any): void {
    this.setStorageToken(JSON.stringify(data.token));
    this.setStorageUser(JSON.stringify(data.user));
  }

  public failToken(): void {
    this.cleanSession();
  }

  public logout(): void {
    this.cleanSession();
  }

  private setStorageToken(value: any): void {
    sessionStorage.setItem('token', value);
    this.authenticationChanged.next(this.isAuthenticated());
  }

  private setStorageUser(value: any): void {
    sessionStorage.setItem('user', value);
    this.authenticationChanged.next(this.isAuthenticated());
  }

  public cleanSession() {
    sessionStorage.clear();
  }

  public getUser(): User {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    return this.user;
  }

  public changePassword(body: ChangePassword): Observable<any> {
    return this.http.post<any>(environment.API_SECURITY + '/api/account/change-password', body)
      .pipe(catchError(this.handleError));
  }

  public forgotPassword(body: ForgotPassword): Observable<any> {
    return this.http.post<any>(environment.API_SECURITY + '/api/account/forgot-password', body)
    .pipe(catchError(this.handleError));
  }

  public resetPassword(body: ResetPassword): Observable<any> {
    return this.http.post<any>(environment.API_SECURITY + '/api/account/reset-password', body)
      .pipe(catchError(this.handleError));
  }
}
