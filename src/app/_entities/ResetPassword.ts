export class ResetPassword {
    email: string;
    code: string;
    password: string;
    passwordConfirmation: string;
}
