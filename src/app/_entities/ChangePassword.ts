export class ChangePassword {
    oldPassword: string;
    password: string;
    confirmPassword: string;
}
